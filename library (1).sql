﻿-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1
-- Время создания: Дек 13 2019 г., 09:06
-- Версия сервера: 10.4.10-MariaDB
-- Версия PHP: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `library`
--

-- --------------------------------------------------------

--
-- Структура таблицы `books`
--

CREATE TABLE `books` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `author` text NOT NULL,
  `year` date NOT NULL,
  `rating_from_1_to_5` int(11) NOT NULL,
  `color` text NOT NULL DEFAULT 'Red',
  `already_read` tinyint(1) NOT NULL DEFAULT 0,
  `pages` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Дамп данных таблицы `books`
--

INSERT INTO `books` (`id`, `name`, `author`, `year`, `rating_from_1_to_5`, `color`, `already_read`, `pages`) VALUES
(1, 'Играть чтобы жить', 'Дмитрий Рус', '2013-02-03', 5, 'Red', 1, 450),
(2, 'Самый странный нуб', 'Каменистый Артем', '2014-03-04', 4, 'Green', 1, 500),
(3, 'К свету', 'Андрей Дьяков', '2010-12-06', 4, 'Black', 1, 475),
(4, 'Путь шамана', 'Василий Маханенко', '2013-07-15', 4, 'Yellow', 1, 510),
(5, 'КОМЭСК-13', 'Дмитрий Рус', '2015-06-15', 5, 'Blue', 1, 390);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `books`
--
ALTER TABLE `books`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `books`
--
ALTER TABLE `books`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
